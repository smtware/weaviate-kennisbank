import json
import weaviate

schema = open('schema.json')
schema = json.loads(schema.read())
w = weaviate.Client("http://host.docker.internal:8080")
w.create_schema(schema)
