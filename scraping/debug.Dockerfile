FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

RUN python3 -m pip install ptvsd 

COPY . .

CMD [ "python3", "-m", "ptvsd", "--host", "0.0.0.0", "--port", "5678", "--wait", "./oai-pmh.py" ]