from oaipmh.client import Client
from oaipmh.metadata import MetadataRegistry, oai_dc_reader
from langdetect import detect
from datetime import datetime, timedelta
import os
import logging
import logging.handlers
import sys


def setup_logger(level):
    logger = logging.getLogger('splunk_weaviate_logger')
    # Prevent the log messages from being duplicated in the python.log file
    logger.propagate = False
    logger.setLevel(level)

    if os.getenv('LOG_FILE') is None:
        handler = logging.StreamHandler(sys.stdout)
    else:
        handler = logging.handlers.RotatingFileHandler(os.getenv(
            'LOG_FILE'), maxBytes=25000000, backupCount=5)

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


URL = 'https://oai.narcis.nl/oai?verb=Identify'

CACHE_DIR = os.getenv('CACHE_DIR', '/tmp')
JSON_DATA_DIR = CACHE_DIR + '/json_oai_data/'

if not os.path.exists(JSON_DATA_DIR):
    os.makedirs(JSON_DATA_DIR)

registry = MetadataRegistry()
registry.registerReader('oai_dc', oai_dc_reader)

log = setup_logger(os.getenv('LOG_LEVEL', 'DEBUG'))
log.info('START SCRAPING ' + URL)

checkpoint_file = open(CACHE_DIR+"/last_datestamp", "a+")
checkpoint_file.seek(0)
try:
    last_timestamp = datetime.strptime(
        checkpoint_file.readline(), "%Y-%m-%d %H:%M:%S")
    last_timestamp = last_timestamp - timedelta(minutes=1)
except ValueError:
    last_timestamp = datetime(2000, 1, 1, 0, 0)

client = Client(URL, registry)
records = client.listRecords(from_=last_timestamp,
                             metadataPrefix='oai_dc',
                             set="oa_publication")
for num, record in enumerate(records):
    checkpoint_file.truncate(0)
    checkpoint_file.write(str(record[0].datestamp()))
    checkpoint_file.flush()

    if record[0].isDeleted():
        log.info('%0.6d %s %s %s' %
                 (num, "SKIP", record[0].identifier(), "Deleted"))
        continue

    try:
        if len(record[1].getMap()['description'][0]) <= len(record[1].getMap()['title'][0]):
            log.info('%0.6d %s %s %s' %
                     (num, "SKIP", record[0].identifier(), "Description shorter than title"))
            continue
    except IndexError:
        log.info('%0.6d %s %s %s' %
                 (num, "SKIP", record[0].identifier(), "Description or title not found"))
        continue

    try:
        lang = detect(", ".join(record[1].getMap()['description']))
        if lang != "nl":
            log.info('%0.6d %s %s %s %s' %
                     (num, "SKIP", record[0].identifier(), "Language not dutch:", lang))
            continue
    except:
        log.info('%0.6d %s %s %s' %
                 (num, "SKIP", record[0].identifier(), "Language not detected"))
        continue

    import re
    identifier = record[0].identifier()
    identifier = re.sub('[^\w\s-]', '', identifier)  # remove non-word chars
    identifier = re.sub('[-\s]+', '', identifier)  # remove whitespace

    import json
    with open(JSON_DATA_DIR+identifier+".json", "w+") as file:
        json.dump(record[1].getMap(), file, indent=2)
    log.debug("Json dumped to: %s", JSON_DATA_DIR+identifier+".json")
    log.info('%0.6d %s %s' %
             (num, "STORE", record[0].identifier()))

checkpoint_file.close()
