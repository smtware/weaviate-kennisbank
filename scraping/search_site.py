import csv
import logging
import logging.handlers
import lxml
import os
import requests
import sys
import xmltodict
import html
import json

from bs4 import BeautifulSoup
from datetime import datetime

# Scraping publications on Narcis.nl
# More info, flow, company etc.
BASE_URL = 'https://www.narcis.nl'
PAGE_URL = 'https://www.narcis.nl/search/coll/publication/Language/NL/access/openAccess/pageable/'
SCRAPING_CONTEXT = 'Narcis'

CACHE_DIR = os.getenv('CACHE_DIR', '/tmp')
XML_DATA_DIR = CACHE_DIR + '/xml_data/'
JSON_DATA_DIR = CACHE_DIR + '/json_data/'
HANDLED_URLS_FILE = CACHE_DIR + "/checkpoint.urls"
MAX_PAGES = int(os.getenv('END_PAGE', 2))
RETRIEVE_XML = bool(os.getenv('RETRIEVE_XML', False))

# Initialize Logger


def setup_logger(level):
    logger = logging.getLogger('splunk_weaviate_logger')
    # Prevent the log messages from being duplicated in the python.log file
    logger.propagate = False
    logger.setLevel(level)

    if os.getenv('LOG_FILE') is None:
        handler = logging.StreamHandler(sys.stdout)
    else:
        handler = logging.handlers.RotatingFileHandler(os.getenv(
            'LOG_FILE'), maxBytes=25000000, backupCount=5)

    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger


def get_file_skip_list():
    skipped_urls = list()
    try:
        with open(HANDLED_URLS_FILE, "r") as file:
            for line in file:
                line = line.strip()
                skipped_urls.append(line)
    except FileNotFoundError:
        pass

    return skipped_urls


def RequestGet(url):
    result = ''

    try:
        result = requests.get(url)

        if (result.status_code == 200):
            log.debug('Request to URL %s SUCCEEDED, status code %s',
                      url, result.status_code)
        else:
            log.error('Request to URL %s FAILED with status code: %s',
                      url, result.status_code)
            result = ''

    except Exception as e:
        log.error('Request to URL %s FAILED. Exception message: %s', url, str(e))

    return result


# Get metadata via link to XML and write to file
def MakeXMLFile(xml_link, file_name):
    xml_url = xml_link.attrs['href']
    log.debug("Try to retrieve XML at: %s", xml_url)
    xml_result = RequestGet(xml_url)

    if xml_result != '':
        xml_document = xml_result.content
        # create soup object with appropriate parser
        xml_document_bs = BeautifulSoup(xml_document, 'lxml-xml')

        # anonimisation disabled for now
        # xml_document_bs.creators.decompose()
        # xml_document_bs.publisher.decompose()
        # xml_document_bs.contributors.decompose()
        # xml_metadata = xml_document_bs.find_all('GetRecord')

        with open(XML_DATA_DIR + file_name, 'w+') as file:
            file.write(xml_document_bs.prettify())
        log.debug('XML dumped to: %s',
                  XML_DATA_DIR + file_name)
    else:
        log.error('Failed to retrieve XML file: %s', xml_url)


def MakeJsonFile(article_bs, file_name):
    meta = article_bs.find_all("div", class_="meta-info")
    article_json = {}
    for row in meta[0].find_all("tr"):
        key = row.find_all("th")

        key_replace = html.unescape(key[0].get_text().strip())
        # if len(key_replace) > 0:
        #    key_replace_new = key_replace[0].lower()
        # if len(key_replace) > 1:
        #    key_replace_new = key_replace_new + key_replace[1:]
        first_word = True
        word_list = key_replace.split(" ")
        word_list_new = []
        for word in word_list:
            if len(word) > 0:
                if first_word:
                    word_new = word[0].lower()
                    if len(word) > 1:
                        word_new = word_new + word[1:]
                    first_word = False
                else:
                    word_new = word[0].upper()
                    if len(word) > 1:
                        word_new = word_new + word[1:]
                word_list_new.append(word_new)

        key_replace_new = "".join(word_list_new)

        value = row.find_all("td")
        if key_replace_new == "Metadata":
            value = value[0].a.attrs['href']
        else:
            value = html.unescape(value[0].get_text())

        if not (value == '' or key_replace_new == ''):
            article_json[key_replace_new] = value

    log.debug('Created Json dict with length: %s', len(article_json))
    with open(JSON_DATA_DIR+file_name, "w+") as file:
        json.dump(article_json, file, indent=2)
    log.debug("Json dumped to: %s", JSON_DATA_DIR+file_name)


def file_name_for_url(url):
    return url.replace('/', '_').replace('%', '_') + '.xml'

# If Article has not been processed before, get URL to article, get XML content and write to file


def ProcessArticle(h2_tag):
    a_tag = h2_tag.find('a')
    article_url = BASE_URL + a_tag.attrs['href']
    file_name = file_name_for_url(a_tag.attrs['href'])

    log.info("process article %s", article_url)
    if file_name in FILE_SKIP_LIST:
        log.debug('Skipping, url in skip list')
    else:
        article_result = RequestGet(article_url)
        if article_result == '':
            log.warn('Failed to retrieve article')
        else:
            article_page = article_result.content
            article_page_bs = BeautifulSoup(article_page, 'lxml')

            # Nederlands artikel?
            nl_found = article_page_bs.find_all('td', string='Nederlands')

            if (len(nl_found) == 1):
                MakeJsonFile(article_page_bs, file_name)
                if RETRIEVE_XML:
                    for xml_link in article_page_bs.find_all('a', string='XML'):
                        log.debug("Found link to XML meta data file")
                        MakeXMLFile(xml_link, file_name)
            else:
                log.debug("Article not Nederlands: SKIPPING")

            # add outputted file to checkpoint file so it will be skipped next time
            with open(HANDLED_URLS_FILE, "a+") as file:
                file.write(file_name+"\n")


log = setup_logger(os.getenv('LOG_LEVEL', 'DEBUG'))
log.info('START SCRAPING ' + BASE_URL)

if not os.path.exists(XML_DATA_DIR):
    os.makedirs(XML_DATA_DIR)
if not os.path.exists(JSON_DATA_DIR):
    os.makedirs(JSON_DATA_DIR)


# Initialize global variables
page_number = int(os.getenv('START_PAGE', 0))
page_result = ''
search_result_bs = []

FILE_SKIP_LIST = get_file_skip_list()

log.info("File skip list inistantiated with %s entries", len(FILE_SKIP_LIST))
log.info("Start at page %s", page_number)
log.info("Finish at page %s", MAX_PAGES)

while page_number < MAX_PAGES:
    # Process Page
    page_url = PAGE_URL + str(page_number) + '/'
    log.info('search result page: %s', page_url)

    page_result = RequestGet(page_url)
    if page_result == '':
        log.warn('Failed to retrieve search result page')
        page_number = page_number + 1
        continue

    # Parse result with bs as xml
    search_result_bs = BeautifulSoup(page_result.content, 'lxml')

    # Loop through all articles
    for h2_tag in search_result_bs.find_all('h2'):
        try:
            ProcessArticle(h2_tag)
        except Exception as e:
            log.error(
                'Processing article %s FAILED. Exception message: %s', str(h2_tag), str(e))

    page_number = int(page_number) + 1
# END while

log.info('END SCRAPING ' + BASE_URL)
