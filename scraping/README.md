# Scraping

## Running the scraper in Docker
We provide a daocker image which contains the scraper.

You can provide several environment variables to this container:

```
LOG_FILE        The full path to the log file which will be used by this container
                    If this is omitted logging is sent to std out.
LOG_LEVEL       Set the level of logging (CRITICAL, FATAL, ERROR, WARN, WARNING, INFO,
                    DEBUG). If this is omitted, defaults to DEBUG
CACHE_DIR       The full path to the directory used for caching during the scrape
START_PAGE      Page number of search results to start scraping
END_PAGE        Page number of search results up to where the scraping should go
RETRIEVE_XML    Switch to also retrieve the xml source files, boolean defaults to False
```

### Develop / debug using docker and VScode
This repo has configurations for debugging python code running in docker using VScode.
This makes running a debug session in a known, well defined environment, an uncomplicated task:

- In VScode, open a python file you would like te debug.
- Set some breakpoints.
- Open the 'Run' view, the side bar will now be filled with debugging panels
- At the top of the side bar, next to "RUN" and a green play-arrow, select "Python: Run in Docker" from the dropdown list
- Press the green play-arrow

Log files and output written to CACHE_DIR will and up in the ```scraping/output``` folder in your local workspace.

The scraper is build on top of the pyoia module. API documentation of this module can be found here: https://raw.githubusercontent.com/infrae/pyoai/master/doc/API.txt

### Create docker image
The following command builds the image and gives it the name ```scraper:1```:
```
docker build -t scraper:1 .
```

### Run scraper
To run the scraper you run the docker image. In order to get data out of the container you can share the ```CACHE_DIR``` with the docker host (your laptop). 

In Dockerland you can share a directory or a file with the ```-v <local>:<remote>``` parameter.

We can for instance put the cache in a subdirectory of our local directory, and conveiniently call this dir ```cache```.
In this case the complete command to run the container, with a ```CACHE_DIR``` on our laptop would be:
```
docker run -it --rm -e CACHE_DIR=/cache -v "$PWD"/cache:/cache scraper:1
```

### Run local code in the container
As part of the development it can be cumbersone to rebuild the container every time a change in the code has been made. An alternative is to mount the local dir which contains the sources in the container and run this code.

Building on the previous complete example, the command to do this would become:
```
docker run -it --rm -e CACHE_DIR=/usr/src/app/cache -v "$PWD:/usr/src/app scraper:1
```

_Do you see what happend here? We mount the local dir on your laptop ```-v "$PWD"``` to the workdir (```:/usr/src/app```) in the container. When the container now runs, it sees the code from the local dir on your laptop instead of the code which was copied in during the ```docker build``` step._