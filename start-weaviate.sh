#!/usr/bin/env bash
set -e          # Abort script at first error, when a command exits with non-zero status 
set -o pipefail # Causes a pipeline to return the exit status of the last command in the 
                # pipe that returned a non-zero return value.
set -u          # Attempt to use undefined variable outputs error message, and forces an exit

DEBUG=${DEBUG:-}

CURL_OPTS="-k -s"

if [[ ! -z $DEBUG ]]; then
	set -x
    CURL_OPTS="-k -sS"
fi

# Download the Weaviate configuration file
curl $CURL_OPTS -O https://raw.githubusercontent.com/semi-technologies/weaviate/0.22.7/docker-compose/runtime/nl/config.yaml
# Download the Weaviate docker-compose file
curl $CURL_OPTS -O https://raw.githubusercontent.com/semi-technologies/weaviate/0.22.7/docker-compose/runtime/nl/docker-compose.yml
# Run Docker Compose
docker-compose up -d

echo -n "Waiting for weaviate to be available  ";
tput civis # cursor invisible
while ! curl $CURL_OPTS http://localhost:8080/v1/meta > /dev/null ; do 
    echo -n "."
    sleep 5;
done
echo " ✓"
tput cnorm

echo "Add schema to weaviate: "
docker-compose -f upload_schema/docker-compose.yml up
